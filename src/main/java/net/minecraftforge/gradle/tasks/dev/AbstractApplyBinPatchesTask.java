package net.minecraftforge.gradle.tasks.dev;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Pack200;
import java.util.regex.Pattern;
import java.util.zip.Adler32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import lzma.sdk.lzma.Decoder;
import lzma.streams.LzmaInputStream;
import net.minecraftforge.gradle.tasks.user.ApplyBinPatchesTask.ClassPatch;

import org.gradle.api.DefaultTask;

import com.google.common.base.Throwables;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.nothome.delta.GDiffPatcher;

public abstract class AbstractApplyBinPatchesTask extends DefaultTask {
	
	protected GDiffPatcher patcher = new GDiffPatcher();

	public abstract void doTask() throws IOException;
	
	public void patchJar(File inJar, File outJar, HashMap<String, ClassPatch> patchList) throws IOException
	{
		if (outJar.exists())
		{
			outJar.delete();
		}
		
		ZipFile in = new ZipFile(inJar);
        final ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outJar)));
        final HashSet<String> entries = new HashSet<String>();
        
        try
        {
            // DO PATCHES
            //log("Patching Class:");
            for (ZipEntry e : Collections.list(in.entries()))
            {
                if (e.getName().contains("META-INF"))
                    continue;
                
                if (e.isDirectory())
                {
                    out.putNextEntry(e);
                }
                else
                {
                    ZipEntry n = new ZipEntry(e.getName());
                    n.setTime(e.getTime());
                    out.putNextEntry(n);
    
                    byte[] data = ByteStreams.toByteArray(in.getInputStream(e));
                    ClassPatch patch = patchList.get(e.getName().replace('\\', '/'));
    
                    if (patch != null)
                    {
                        //log("\t%s (%s) (input size %d)", patch.targetClassName, patch.sourceClassName, data.length);
                        int inputChecksum = adlerHash(data);
                        if (patch.inputChecksum != inputChecksum)
                        {
                            throw new RuntimeException(String.format("There is a binary discrepency between the expected input class %s (%s) and the actual class. Checksum on disk is %x, in patch %x. Things are probably about to go very wrong. Did you put something into the jar file?", patch.targetClassName, patch.sourceClassName, inputChecksum, patch.inputChecksum));
                        }
                        synchronized (patcher)
                        {
                            data = patcher.patch(data, patch.patch);
                        }
                    }
    
                    out.write(data);
                }
                
                // add the names to the hashset
                entries.add(e.getName());
            }
            
            // COPY DATA
            this.copyClasses(out, entries);
        }
        finally
        {
            //classesIn.close();
            in.close();
            out.close();
        }
	}
	
	public abstract void copyClasses(ZipOutputStream out, HashSet<String> entries) throws IOException;
	
	private int adlerHash(byte[] input)
    {
        Adler32 hasher = new Adler32();
        hasher.update(input);
        return (int) hasher.getValue();
    }
	
	public void setupBinPatches(HashMap<String, ClassPatch> patchList, String subfolderName, ZipFile pack, ZipEntry packedBinPatches)
	{
        Pattern matcher = Pattern.compile(String.format("binpatch/" + subfolderName + "/.*.binpatch"));

        JarInputStream jis;
        
        InputStream binpatchesCompressed;
        try
        {
        	binpatchesCompressed = pack.getInputStream(packedBinPatches);
            LzmaInputStream binpatchesDecompressed = new LzmaInputStream(binpatchesCompressed, new Decoder());
            ByteArrayOutputStream jarBytes = new ByteArrayOutputStream();
            JarOutputStream jos = new JarOutputStream(jarBytes);
            Pack200.newUnpacker().unpack(binpatchesDecompressed, jos);
            jis = new JarInputStream(new ByteArrayInputStream(jarBytes.toByteArray()));
        }
        catch (Exception e)
        {
            throw Throwables.propagate(e);
        }

        //log("Reading Patches:");
        do
        {
            try
            {
                JarEntry entry = jis.getNextJarEntry();
                if (entry == null)
                {
                    break;
                }

                if (matcher.matcher(entry.getName()).matches())
                {
                    ClassPatch cp = readPatch(entry, jis);
                    patchList.put(cp.sourceClassName.replace('.', '/') + ".class", cp);
                }
                else
                {
                    jis.closeEntry();
                }
            }
            catch (IOException e)
            {
            }
        } while (true);
        //log("Read %d binary patches", patchlist.size());
        //log("Patch list :\n\t%s", Joiner.on("\n\t").join(patchlist.entrySet()));
    }
	
	private ClassPatch readPatch(JarEntry patchEntry, JarInputStream jis) throws IOException
    {
        //log("\t%s", patchEntry.getName());
        ByteArrayDataInput input = ByteStreams.newDataInput(ByteStreams.toByteArray(jis));
        
        String name = input.readUTF();
        String sourceClassName = input.readUTF();
        String targetClassName = input.readUTF();
        boolean exists = input.readBoolean();
        int inputChecksum = 0;
        if (exists)
        {
            inputChecksum = input.readInt();
        }
        int patchLength = input.readInt();
        byte[] patchBytes = new byte[patchLength];
        input.readFully(patchBytes);

        return new ClassPatch(name, sourceClassName, targetClassName, exists, inputChecksum, patchBytes);
    }
}

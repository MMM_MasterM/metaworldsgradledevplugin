package net.minecraftforge.gradle.tasks.dev;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

import net.minecraftforge.gradle.delayed.DelayedFile;
import net.minecraftforge.gradle.delayed.DelayedFileTree;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.io.ByteStreams;
import com.google.common.io.Files;
import com.google.common.io.LineProcessor;

//This task takes the reobfed classes of those which were patched and the vanilla classes of those which were not
public class ObfJarSelectClasses extends DefaultTask {
	
	@InputFile
    private DelayedFile             cleanClient;

    @InputFile
    private DelayedFile             cleanServer;

    @InputFile
    private DelayedFile             cleanMerged;

    @InputFile
    private DelayedFile             dirtyJar;
    
    private List<DelayedFileTree>   patchList    = new ArrayList<DelayedFileTree>();
    
    @InputFile
    private DelayedFile             srg;

    @OutputFile
    private DelayedFile             outJarClient;
    
    @OutputFile
    private DelayedFile             outJarServer;
    
    @OutputFile
    private DelayedFile             outJarMerged;
    
    private HashMap<String, String> obfMapping   = new HashMap<String, String>();
    private HashMap<String, String> srgMapping   = new HashMap<String, String>();
    private ArrayListMultimap<String, String> innerClasses   = ArrayListMultimap.create();
    
    private Set<String>             patchedFiles = new HashSet<String>();
	
	@TaskAction
    public void doTask() throws Exception
    {
		loadMappings();
		
		for (DelayedFileTree tree : patchList)
        {
            for (File patch : tree.call().getFiles())
            {
                String name = patch.getName().replace(".java.patch", "");
                String obfName = srgMapping.get(name);
                patchedFiles.add(obfName);
                addInnerClasses(name, patchedFiles);
            }
        }
		
		selectAndPackClasses(getOutJarClient(), getCleanClient(), getDirtyJar());
		selectAndPackClasses(getOutJarServer(), getCleanServer(), getDirtyJar());
		selectAndPackClasses(getOutJarMerged(), getCleanMerged(), getDirtyJar());
    }
	
	private void addInnerClasses(String parent, Set<String> patchList)
    {
        // Recursively add inner classes to the list of patches - this will mean we ship anything affected by "access$" changes
        for (String inner : innerClasses.get(parent))
        {
            patchList.add(srgMapping.get(inner));
            addInnerClasses(inner, patchList);
        }
    }
	
	private void loadMappings() throws Exception
    {
        Files.readLines(getSrg(), Charset.defaultCharset(), new LineProcessor<String>() {

            Splitter splitter = Splitter.on(CharMatcher.anyOf(": ")).omitEmptyStrings().trimResults();

            @Override
            public boolean processLine(String line) throws IOException
            {
                if (!line.startsWith("CL"))
                {
                    return true;
                }

                String[] parts = Iterables.toArray(splitter.split(line), String.class);
                obfMapping.put(parts[1], parts[2]);
                String srgName = parts[2].substring(parts[2].lastIndexOf('/') + 1);
                srgMapping.put(srgName, parts[1]);
                int innerDollar = srgName.lastIndexOf('$');
                if (innerDollar > 0)
                {
                    String outer = srgName.substring(0, innerDollar);
                    innerClasses.put(outer, srgName);
                }
                return true;
            }

            @Override
            public String getResult()
            {
                return null;
            }
        });
    }

	
	private void selectAndPackClasses(File outJar, File base, File target) throws Exception
	{
		JarOutputStream out = new JarOutputStream(new FileOutputStream(outJar));
		
		JarFile cleanJ = new JarFile(base);
        JarFile dirtyJ = new JarFile(target);
        
        Enumeration<JarEntry> dirtyEntries = dirtyJ.entries();
        while(dirtyEntries.hasMoreElements())
        {
        	JarEntry curEntry = dirtyEntries.nextElement();
        	
        	String curFullEntryName = curEntry.getName();
        	
        	if (!curFullEntryName.endsWith(".class"))//only take class files
        		continue;
        	
        	String curClassNameWithPath = curFullEntryName.replace(".class", "");
        	String[] nameSplit = curClassNameWithPath.split("/", -1);
    		String curClassName = nameSplit[nameSplit.length - 1];
        	
        	boolean takeDirty = false;
        	
        	if (patchedFiles.contains(curClassName))
        		takeDirty = true;
        	
        	if (!takeDirty)
        	{
        		JarEntry cleanEntry = cleanJ.getJarEntry(curFullEntryName);
        		
        		if (cleanEntry != null)
        		{
        			JarEntry newEntry = new JarEntry(cleanEntry.getName());
        			newEntry.setTime(cleanEntry.getTime());
        			out.putNextEntry(newEntry);
        			out.write(ByteStreams.toByteArray(cleanJ.getInputStream(cleanEntry)));
        		}
        		else 
        			takeDirty = true;
        	}
        	
        	if (takeDirty)
        	{
        		JarEntry newEntry = new JarEntry(curFullEntryName);
    			newEntry.setTime(curEntry.getTime());
    			out.putNextEntry(newEntry);
    			out.write(ByteStreams.toByteArray(dirtyJ.getInputStream(curEntry)));
        	}
        }
        
        dirtyJ.close();
        cleanJ.close();
        
        out.close();
	}
	
	public void addPatchList(DelayedFileTree patchList)
    {
        this.patchList.add(patchList);
    }
	
	public File getCleanClient()
    {
        return cleanClient.call();
    }

    public void setCleanClient(DelayedFile cleanClient)
    {
        this.cleanClient = cleanClient;
    }

    public File getCleanServer()
    {
        return cleanServer.call();
    }

    public void setCleanServer(DelayedFile cleanServer)
    {
        this.cleanServer = cleanServer;
    }

    public File getCleanMerged()
    {
        return cleanMerged.call();
    }

    public void setCleanMerged(DelayedFile cleanMerged)
    {
        this.cleanMerged = cleanMerged;
    }

    public File getDirtyJar()
    {
        return dirtyJar.call();
    }

    public void setDirtyJar(DelayedFile dirtyJar)
    {
        this.dirtyJar = dirtyJar;
    }
    
    public File getOutJarClient()
    {
        return outJarClient.call();
    }

    public void setOutJarClient(DelayedFile outJar)
    {
        this.outJarClient = outJar;
    }
    
    public File getOutJarServer()
    {
        return outJarServer.call();
    }

    public void setOutJarServer(DelayedFile outJar)
    {
        this.outJarServer = outJar;
    }
    
    public File getOutJarMerged()
    {
        return outJarMerged.call();
    }

    public void setOutJarMerged(DelayedFile outJar)
    {
        this.outJarMerged = outJar;
    }

    public File getSrg()
    {
        return srg.call();
    }

    public void setSrg(DelayedFile srg)
    {
        this.srg = srg;
    }
}

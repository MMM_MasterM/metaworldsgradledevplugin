package net.minecraftforge.gradle.tasks.dev;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import net.minecraftforge.gradle.delayed.DelayedFile;
import net.minecraftforge.gradle.tasks.user.ApplyBinPatchesTask.ClassPatch;

import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import com.google.common.collect.Maps;
import com.google.common.io.ByteStreams;

public class ApplyUserDevBinPatchesTask extends AbstractApplyBinPatchesTask {
	@InputFile
	DelayedFile inJarMerged;
	
	@InputFile
	DelayedFile userDevJar;
	
	@OutputFile
	DelayedFile outJarMerged;

	private HashMap<String, ClassPatch> devPatchList = Maps.newHashMap();
	
	@TaskAction
	@Override
    public void doTask() throws IOException
    {
		setup();
		
		patchJar(this.getInJarMerged(), this.getOutJarMerged(), this.devPatchList);
    }
	
	public void setup()
    {
		try {
            ZipFile zip = new ZipFile(this.getUserDevJar());
            
            Enumeration<ZipEntry> entries = (Enumeration<ZipEntry>)zip.entries();
            while(entries.hasMoreElements()) {
                ZipEntry curEntry = entries.nextElement();
                String filename = curEntry.getName();
                
                if (filename.equals("devbinpatches.pack.lzma"))
                {
                    setupBinPatches(this.devPatchList, "merged", zip, curEntry);
                    break;
                }
            }
            zip.close();
        } catch(Exception e) { }
    }
	
	@Override
	public void copyClasses(ZipOutputStream out, HashSet<String> entries) throws IOException
	{
		ZipFile zip = new ZipFile(this.getUserDevJar());
        
        Enumeration<ZipEntry> zipEntries = (Enumeration<ZipEntry>)zip.entries();
        while(zipEntries.hasMoreElements()) {
            ZipEntry curEntry = zipEntries.nextElement();
            String filename = curEntry.getName();
            
            if (filename.equals("binaries.jar"))
            {
            	copyClassesFromBinariesEntry(out, entries, zip, curEntry);
                break;
            }
        }
        zip.close();
	}
	
	public void copyClassesFromBinariesEntry(ZipOutputStream out, HashSet<String> entries, ZipFile pack, ZipEntry packedBinaries) throws IOException
	{
		InputStream compressedBinaries = pack.getInputStream(packedBinaries);
		
		ZipInputStream classesIn = new ZipInputStream(compressedBinaries);
		
		try
		{
			ZipEntry entry = null;
	        while ((entry = classesIn.getNextEntry()) != null)
	        {
	            if (entries.contains(entry.getName()))
	                continue;
	            
	            if (!entry.getName().endsWith(".class") && !entry.isDirectory())
	            	continue;
	            
	            out.putNextEntry(entry);
	            out.write(ByteStreams.toByteArray(classesIn));
	            entries.add(entry.getName());
	        }
		}
		finally
		{
			classesIn.close();
		}
	}
	
	public File getInJarMerged()
    {
        return inJarMerged.call();
    }

    public void setInJarMerged(DelayedFile inJar)
    {
        this.inJarMerged = inJar;
    }
    
    public File getUserDevJar()
    {
        return userDevJar.call();
    }

    public void setUserDevJar(DelayedFile userDevJarParam)
    {
        this.userDevJar = userDevJarParam;
    }

    public File getOutJarMerged()
    {
        return outJarMerged.call();
    }

    public void setOutJarMerged(DelayedFile outJar)
    {
        this.outJarMerged = outJar;
    }
}

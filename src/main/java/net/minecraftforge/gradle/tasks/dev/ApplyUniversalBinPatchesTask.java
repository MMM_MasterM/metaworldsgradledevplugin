package net.minecraftforge.gradle.tasks.dev;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import net.minecraftforge.gradle.delayed.DelayedFile;
import net.minecraftforge.gradle.tasks.user.ApplyBinPatchesTask.ClassPatch;

import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import com.google.common.collect.Maps;
import com.google.common.io.ByteStreams;

public class ApplyUniversalBinPatchesTask extends AbstractApplyBinPatchesTask {
	@InputFile
	DelayedFile inJarClient;
	
	@InputFile
	DelayedFile inJarServer;
	
	@InputFile
	DelayedFile universalJar;
	
	@OutputFile
	DelayedFile outJarClient;
	
	@OutputFile
	DelayedFile outJarServer;

	private HashMap<String, ClassPatch> clientPatchList = Maps.newHashMap();
	private HashMap<String, ClassPatch> serverPatchList = Maps.newHashMap();
	
	@TaskAction
	@Override
    public void doTask() throws IOException
    {
		setup();
		
		patchJar(this.getInJarClient(), this.getOutJarClient(), this.clientPatchList);
		patchJar(this.getInJarServer(), this.getOutJarServer(), this.serverPatchList);
    }
	
	public void setup()
    {
		try {
            ZipFile zip = new ZipFile(this.getUniversalJar());
            
            Enumeration<ZipEntry> entries = (Enumeration<ZipEntry>)zip.entries();
            while(entries.hasMoreElements()) {
                ZipEntry curEntry = entries.nextElement();
                String filename = curEntry.getName();
                
                if (filename.equals("binpatches.pack.lzma"))
                {
                    setupBinPatches(this.clientPatchList, "client", zip, curEntry);
                    setupBinPatches(this.serverPatchList, "server", zip, curEntry);
                    break;
                }
            }
            zip.close();
        } catch(Exception e) { }
    }
	
	@Override
	public void copyClasses(ZipOutputStream out, HashSet<String> entries) throws IOException
	{
		ZipInputStream classesIn = new ZipInputStream(new FileInputStream(this.getUniversalJar()));
		
		try
		{
			ZipEntry entry = null;
	        while ((entry = classesIn.getNextEntry()) != null)
	        {
	            if (entries.contains(entry.getName()))
	                continue;
	            
	            if (!entry.getName().endsWith(".class") && !entry.isDirectory())
	            	continue;
	            
	            out.putNextEntry(entry);
	            out.write(ByteStreams.toByteArray(classesIn));
	            entries.add(entry.getName());
	        }
		}
		finally
		{
			classesIn.close();
		}
	}
	
	public File getInJarClient()
    {
        return inJarClient.call();
    }

    public void setInJarClient(DelayedFile inJar)
    {
        this.inJarClient = inJar;
    }
    
    public File getInJarServer()
    {
        return inJarServer.call();
    }

    public void setInJarServer(DelayedFile inJar)
    {
        this.inJarServer = inJar;
    }
    
    public File getUniversalJar()
    {
        return universalJar.call();
    }

    public void setUniversalJar(DelayedFile universalJarParam)
    {
        this.universalJar = universalJarParam;
    }

    public File getOutJarClient()
    {
        return outJarClient.call();
    }

    public void setOutJarClient(DelayedFile outJar)
    {
        this.outJarClient = outJar;
    }
    
    public File getOutJarServer()
    {
        return outJarServer.call();
    }

    public void setOutJarServer(DelayedFile outJar)
    {
        this.outJarServer = outJar;
    }
}

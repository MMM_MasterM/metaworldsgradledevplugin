package net.minecraftforge.gradle.tasks.dev;

import java.io.File;
import java.io.IOException;

import net.minecraftforge.gradle.delayed.DelayedFile;

import org.gradle.api.tasks.InputDirectory;

public class GeneratePatchesAfterForge extends GeneratePatches {
	@InputDirectory
    DelayedFile originalNewSourcesDir;
	
	private boolean toplevelCall = true;
	private boolean workingOnNewSources;
	
	@Override
	public void processDir(File dir) throws IOException
    {
		if (toplevelCall)
		{
			toplevelCall = false;
			workingOnNewSources = true;
			processDir(getOriginalNewSourcesDir());
			workingOnNewSources = false;
		}
		
		super.processDir(dir);
    }
	
	@Override
	public File getOriginalDir()
	{
		if (this.workingOnNewSources)
			return this.getOriginalNewSourcesDir();
		else
			return super.getOriginalDir();
	}
	
	public File getOriginalNewSourcesDir()
    {
        return originalNewSourcesDir.call();
    }

    public void setOriginalNewSourcesDir(DelayedFile originalDir)
    {
        this.originalNewSourcesDir = originalDir;
    }
}

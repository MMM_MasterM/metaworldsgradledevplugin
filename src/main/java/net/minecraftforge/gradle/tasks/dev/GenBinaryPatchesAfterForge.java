package net.minecraftforge.gradle.tasks.dev;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.gradle.api.tasks.TaskAction;

import net.minecraftforge.gradle.delayed.DelayedFileTree;
import net.minecraftforge.gradle.tasks.dev.GenBinaryPatches;

public class GenBinaryPatchesAfterForge extends GenBinaryPatches {
	@SuppressWarnings("unchecked")
    @TaskAction
	@Override
	public void doTask() throws Exception
    {
		Class<?> origClass = GenBinaryPatches.class;
		
		Method loadMappingsMethod = origClass.getDeclaredMethod("loadMappings", (Class<?>[])null);
		loadMappingsMethod.setAccessible(true);
		loadMappingsMethod.invoke(this, (Object[])null);
        //loadMappings();
		
		//NEW AFTER FORGE start
        loadForgeClassNames();
        //NEW AFTER FORGE end
		
		Field patchListField = origClass.getDeclaredField("patchList");
		patchListField.setAccessible(true);
		List<DelayedFileTree> patchList = (List<DelayedFileTree>)patchListField.get(this);
		
		Field srgMappingField = origClass.getDeclaredField("srgMapping");
		srgMappingField.setAccessible(true);
		HashMap<String, String> srgMapping = (HashMap<String, String>)srgMappingField.get(this);
		
		Field patchedFilesField = origClass.getDeclaredField("patchedFiles");
		patchedFilesField.setAccessible(true);
		Set<String> patchedFiles = (Set<String>)patchedFilesField.get(this);
		
		Method addInnerClassesMethod = origClass.getDeclaredMethod("addInnerClasses", String.class, Set.class);
		addInnerClassesMethod.setAccessible(true);

        for (DelayedFileTree tree : patchList)
        {
            for (File patch : tree.call().getFiles())
            {
                String name = patch.getName().replace(".java.patch", "");
                String obfName = srgMapping.get(name);
                patchedFiles.add(obfName);
                addInnerClassesMethod.invoke(this, name, patchedFiles);
                //addInnerClasses(name, patchedFiles);
            }
        }

        HashMap<String, byte[]> runtime = new HashMap<String, byte[]>();
        HashMap<String, byte[]> devtime = new HashMap<String, byte[]>();
        
        Method createBinPatchesMethod = origClass.getDeclaredMethod("createBinPatches", HashMap.class, String.class, File.class, File.class);
        createBinPatchesMethod.setAccessible(true);

        createBinPatchesMethod.invoke(this, runtime, "client/", getCleanClient(), getDirtyJar());
        //createBinPatches(runtime, "client/", getCleanClient(), getDirtyJar());
        createBinPatchesMethod.invoke(this, runtime, "server/", getCleanServer(), getDirtyJar());
        //createBinPatches(runtime, "server/", getCleanServer(), getDirtyJar());
        createBinPatchesMethod.invoke(this, devtime, "merged/", getCleanMerged(), getDirtyJar());
        //createBinPatches(devtime, "merged/", getCleanMerged(), getDirtyJar());

        Method createPatchJarMethod = origClass.getDeclaredMethod("createPatchJar", HashMap.class);
        createPatchJarMethod.setAccessible(true);
        
        Method pack200Method = origClass.getDeclaredMethod("pack200", byte[].class);
        pack200Method.setAccessible(true);
        
        Method compressMethod = origClass.getDeclaredMethod("compress", byte[].class);
        compressMethod.setAccessible(true);
        
        byte[] runtimedata = (byte[])createPatchJarMethod.invoke(this, runtime);
        //byte[] runtimedata = createPatchJar(runtime);
        runtimedata = (byte[])pack200Method.invoke(this, runtimedata);
        //runtimedata = pack200(runtimedata);
        runtimedata = (byte[])compressMethod.invoke(this, runtimedata);
        //runtimedata = compress(runtimedata);

        byte[] devtimedata = (byte[])createPatchJarMethod.invoke(this, devtime);
        //byte[] devtimedata = createPatchJar(devtime);
        devtimedata = (byte[])pack200Method.invoke(this, devtimedata);
        //devtimedata = pack200(devtimedata);
        devtimedata = (byte[])compressMethod.invoke(this, devtimedata);
        //devtimedata = compress(devtimedata);
        
        Method buildOutputMethod = origClass.getDeclaredMethod("buildOutput", byte[].class, byte[].class);
        buildOutputMethod.setAccessible(true);

        buildOutputMethod.invoke(this, runtimedata, devtimedata);
        //buildOutput(runtimedata, devtimedata);
    }
	
	@SuppressWarnings("unchecked")
	private void loadForgeClassNames() throws Exception
	{
		Class<?> origClass = GenBinaryPatches.class;
		HashMap<String, String> obfMapping = null;
		HashMap<String, String> srgMapping = null;
		
		try {
			Field obfMappingField = origClass.getDeclaredField("obfMapping");
			Field srgMappingField = origClass.getDeclaredField("srgMapping");
			obfMappingField.setAccessible(true);
			srgMappingField.setAccessible(true);
			try {
				obfMapping = (HashMap<String, String>)obfMappingField.get(this);
				srgMapping = (HashMap<String, String>)srgMappingField.get(this);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		
		JarFile in = new JarFile(getCleanMerged());
		
		for (JarEntry e : Collections.list(in.entries()))
		{
			if(!e.getName().endsWith(".class"))
				continue;
			
			String curClassName = e.getName().replace(".class", "");
			
			if(!obfMapping.containsKey(curClassName))
			{
				obfMapping.put(curClassName, curClassName);
			}
			
			if(!srgMapping.containsValue(curClassName))
			{
				String srgName = curClassName.substring(curClassName.lastIndexOf('/') + 1);
				
				if (!srgMapping.containsKey(srgName))
					srgMapping.put(srgName, curClassName);
			}
		}
		
		in.close();
	}
}

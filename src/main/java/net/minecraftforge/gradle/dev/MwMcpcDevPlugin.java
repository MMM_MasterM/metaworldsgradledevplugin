package net.minecraftforge.gradle.dev;

import groovy.lang.Closure;

import java.util.HashMap;

import static net.minecraftforge.gradle.dev.DevConstants.*;
import static net.minecraftforge.gradle.dev.MwDevConstants.*;
import net.minecraftforge.gradle.common.Constants;
import net.minecraftforge.gradle.delayed.DelayedFile;
import net.minecraftforge.gradle.delayed.DelayedFileTree;
import net.minecraftforge.gradle.delayed.DelayedString;
import net.minecraftforge.gradle.delayed.DelayedBase.IDelayedResolver;
import net.minecraftforge.gradle.tasks.PatchJarTask;
import net.minecraftforge.gradle.tasks.abstractutil.DelayedJar;
import net.minecraftforge.gradle.tasks.abstractutil.ExtractTask;
import net.minecraftforge.gradle.tasks.dev.ApplyUniversalBinPatchesTask;
import net.minecraftforge.gradle.tasks.dev.GenBinaryPatchesAfterForge;
import net.minecraftforge.gradle.tasks.dev.GenDevProjectsTask;
import net.minecraftforge.gradle.tasks.dev.GeneratePatches;
import net.minecraftforge.gradle.tasks.dev.GeneratePatchesAfterForge;
import net.minecraftforge.gradle.tasks.dev.ObfJarSelectClasses;
import net.minecraftforge.gradle.tasks.dev.ObfuscateTask;
import net.minecraftforge.gradle.tasks.dev.SubprojectTask;

import org.gradle.api.DefaultTask;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.java.archives.Manifest;
import org.gradle.api.tasks.bundling.Zip;

public class MwMcpcDevPlugin implements Plugin<Project>, IDelayedResolver<DevExtension> {
	
	protected static final String[] JAVA_FILES = new String[] { "**.java", "*.java", "**/*.java" };
	
    public Project    project;
    
    static final String metaworldsDir = "metaworlds";
    
    @Override
    public final void apply(Project arg)
    {
    	project = arg;
    	
    	applyPlugin();
    }
    
    public void applyPlugin()
    {
    	createSetupTasks();
    	createMiscTasks();
    	
    	GenDevProjectsTask task = makeTask("generateProjectMetaWorlds", GenDevProjectsTask.class);
    	{
    		task.setJson(delayedFile(METAWORLDS_MCPC_JSON_DEV));
            task.setTargetDir(delayedFile(ECLIPSE_METAWORLDS));

            task.addSource(delayedFile(ECLIPSE_METAWORLDS_SRC));
            task.addSource(delayedFile(METAWORLDS_SOURCES));
            task.addTestSource(delayedFile(METAWORLDS_TEST_SOURCES));

            task.addResource(delayedFile(ECLIPSE_METAWORLDS_RES));
            task.addResource(delayedFile(METAWORLDS_RESOURCES));
            //task.addResource(delayedFile(EXTRACTED_RES));
            task.addTestResource(delayedFile(METAWORLDS_TEST_RES));

            task.dependsOn(/*"extractRes", */"extractNatives","createVersionPropertiesFML");
    	}
    	
    	(project.getTasksByName("generateProjects", false).toArray(new Task[0])[0]).dependsOn("generateProjectMetaWorlds");
    	(project.getTasksByName("eclipse", false).toArray(new Task[0])[0]).dependsOn("eclipseMetaworlds");
    	
    	ObfuscateTask obf = makeTask("obfuscateJarMW", ObfuscateTask.class);
        {
            obf.setSrg(delayedFile(MCP_2_NOTCH_SRG));
            obf.setExc(delayedFile(JOINED_EXC));
            obf.setReverse(false);
            obf.setPreFFJar(delayedFile(JAR_SRG_FORGE));
            obf.setOutJar(delayedFile(MW_REOBF_TMP));
            obf.setBuildFile(delayedFile(ECLIPSE_METAWORLDS + "/build.gradle"));
            obf.setMethodsCsv(delayedFile(METHODS_CSV));
            obf.setFieldsCsv(delayedFile(FIELDS_CSV));
            obf.dependsOn("generateProjects", "extractMcpcSources", "genSrgs");
        }
        
        ObfJarSelectClasses obf2 = makeTask("selectPatchedAndCleanClassesOnly", ObfJarSelectClasses.class);
        {
        	obf2.setCleanClient(delayedFile(Constants.JAR_CLIENT_FRESH));
        	obf2.setCleanServer(delayedFile(Constants.JAR_SERVER_FRESH));
        	obf2.setCleanMerged(delayedFile(Constants.JAR_MERGED));
        	obf2.setDirtyJar(delayedFile(REOBF_TMP));
        	obf2.setOutJarClient(delayedFile(FORGE_CLEANED_REOBF_TMP_CLIENT));
        	obf2.setOutJarServer(delayedFile(FORGE_CLEANED_REOBF_TMP_SERVER));
        	obf2.setOutJarMerged(delayedFile(FORGE_CLEANED_REOBF_TMP_MERGED));
        	obf2.setSrg(delayedFile(JOINED_SRG));
        	obf2.addPatchList(delayedFileTree(MCPC_PATCH_DIR));
        	obf2.addPatchList(delayedFileTree(FORGE_PATCH_DIR));
        	obf2.addPatchList(delayedFileTree(FML_PATCH_DIR));
        	obf2.dependsOn("obfuscateJar");
        }
        
        ApplyUniversalBinPatchesTask task4 = makeTask("patchToOnSiteBinaries", ApplyUniversalBinPatchesTask.class);
        {
        	task4.setInJarClient(delayedFile(Constants.JAR_CLIENT_FRESH));
        	task4.setInJarServer(delayedFile(Constants.JAR_SERVER_FRESH));
        	task4.setOutJarClient(delayedFile(FORGE_ON_SITE_CLIENT));
        	task4.setOutJarServer(delayedFile(FORGE_ON_SITE_SERVER));
        	task4.setUniversalJar(delayedFile(MCPC_UNIVERSAL_JAR));
        }
        
        GenBinaryPatchesAfterForge task3 = makeTask("genMetaWorldsBinPatches", GenBinaryPatchesAfterForge.class);
        {
            task3.setCleanClient(delayedFile(FORGE_ON_SITE_CLIENT));
            task3.setCleanServer(delayedFile(FORGE_ON_SITE_SERVER));
            task3.setCleanMerged(delayedFile(FORGE_CLEANED_REOBF_TMP_MERGED));
            task3.setDirtyJar(delayedFile(MW_REOBF_TMP));
            task3.setDeobfDataLzma(delayedFile(DEOBF_DATA));
            task3.setOutJar(delayedFile(MW_BINPATCH_TMP));
            task3.setSrg(delayedFile(JOINED_SRG));
            task3.addPatchList(delayedFileTree(METAWORLDS_PATCH_DIR));
            task3.dependsOn("obfuscateJarMW", "compressDeobfData", "selectPatchedAndCleanClassesOnly", "patchToOnSiteBinaries");
        }
        
        final DelayedJar mwPackage = makeTask("packageMetaworlds", DelayedJar.class);
    	{
    		mwPackage.setClassifier("metaworlds");
    		
    		mwPackage.from(delayedZipTree(MW_BINPATCH_TMP));
    		
    		mwPackage.setIncludeEmptyDirs(false);
    		
    		mwPackage.setManifest(new Closure<Object>(project)
    	            {
    	                public Object call()
    	                {
    	                    Manifest mani = (Manifest) getDelegate();
    	                    mani.getAttributes().put("FMLCorePlugin", delayedString("metaworlds.patcher.MetaworldsFMLLoadingPlugin").call());
    	                    //mani.getAttributes().put("TweakClass", delayedString("{FML_TWEAK_CLASS}").call());
    	                    //mani.getAttributes().put("Class-Path", getServerClassPath(delayedFile(JSON_REL).call()));
    	                    return null;
    	                }
    	            });
    		
    		mwPackage.setDestinationDir(delayedFile("{BUILD_DIR}/distributions").call());
    		mwPackage.dependsOn("genMetaWorldsBinPatches");
    		mwPackage.setGroup("MetaWorlds");
    	}
    }
    
    public void createMiscTasks()
    {
    	GeneratePatchesAfterForge task2 = makeTask("genMetaworldsPatches", GeneratePatchesAfterForge.class);
        {
            task2.setPatchDir(delayedFile(METAWORLDS_PATCH_DIR));
            task2.setOriginalDir(delayedFile(ECLIPSE_MCPC_SRC));
            task2.setOriginalNewSourcesDir(delayedFile(MCPC_SOURCES));
            task2.setChangedDir(delayedFile(ECLIPSE_METAWORLDS_SRC));
            task2.setOriginalPrefix("../src-base/minecraft");
            task2.setChangedPrefix("../src-work/minecraft");
            task2.setGroup("MetaWorlds");
        }
    }
    
    public void createSetupTasks()
    {
    	Zip task5 = makeTask("mcpcInjectJarForMw", Zip.class);//copy mcpc's own source files into the archive with the mcpc-patched minecraft files
    	{
    		task5.from(delayedFileTree(MCPC_SOURCES));
    		task5.from(delayedFileTree(MCPC_RESOURCES));
    		task5.from(delayedZipTree(ZIP_PATCHED_MCPC));
    		
    		//See ZIP_MCPCINJECTED_METAWORLDS
    		task5.setArchiveName("minecraft_mcpcinjected.zip");
    		task5.setDestinationDir(delayedFile("{BUILD_DIR}/mwTmp").call());
    		
    		task5.dependsOn("mcpcPatchJar");
    	}
    	
    	PatchJarTask task4 = makeTask("metaworldsPatchJar", PatchJarTask.class);
        {
            task4.setInJar(delayedFile(ZIP_MCPCINJECTED_METAWORLDS));
            task4.setOutJar(delayedFile(ZIP_PATCHED_METAWORLDS));
            task4.setInPatches(delayedFile(METAWORLDS_PATCH_DIR));
            task4.setDoesCache(false);
            task4.setMaxFuzz(2);
            task4.dependsOn("mcpcInjectJarForMw");
        }
        
        ExtractTask task = makeTask("extractMetaworldsResources", ExtractTask.class);
        {
            task.exclude(JAVA_FILES);
            task.from(delayedFile(ZIP_PATCHED_METAWORLDS));
            task.into(delayedFile(ECLIPSE_METAWORLDS_RES));
            task.dependsOn("metaworldsPatchJar", "extractWorkspace");
        }
        
        task = makeTask("extractMetaworldsSources", ExtractTask.class);
        {
            task.include(JAVA_FILES);
            task.from(delayedFile(ZIP_PATCHED_METAWORLDS));
            task.into(delayedFile(ECLIPSE_METAWORLDS_SRC));
            task.dependsOn("extractMetaworldsResources");
        }
        
        SubprojectTask task1 = makeTask("eclipseMetaworlds", SubprojectTask.class);
        {
            task1.setBuildFile(delayedFile(ECLIPSE_METAWORLDS + "/build.gradle"));
            task1.setTasks("eclipse");
            task1.dependsOn("extractMetaworldsSources", "generateProjects");
        }
    }
    
	protected DelayedString delayedString(String path)
    {
        return new DelayedString(project, path, this);
    }
    
    protected DelayedFile delayedFile(String path)
    {
        return new DelayedFile(project, path, this);
    }
    
    protected DelayedFileTree delayedFileTree(String path)
    {
        return new DelayedFileTree(project, path, this);
    }
    
    protected DelayedFileTree delayedZipTree(String path)
    {
        return new DelayedFileTree(project, path, true, this);
    }

    public DefaultTask makeTask(String name)
    {
        return makeTask(name, DefaultTask.class);
    }

    public <T extends Task> T makeTask(String name, Class<T> type)
    {
        return makeTask(project, name, type);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Task> T makeTask(Project proj, String name, Class<T> type)
    {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("name", name);
        map.put("type", type);
        return (T) proj.task(map, name);
    }

	@Override
	public String resolve(String pattern, Project arg1, DevExtension exten) {
		/*if (version != null)
            pattern = pattern.replace("{ASSET_INDEX}", version.getAssets());*/
		
		pattern = pattern.replace("{MAIN_CLASS}", exten.getMainClass());
        pattern = pattern.replace("{FML_TWEAK_CLASS}", exten.getTweakClass());
        pattern = pattern.replace("{INSTALLER_VERSION}", exten.getInstallerVersion());
        pattern = pattern.replace("{FML_DIR}", exten.getFmlDir());
        pattern = pattern.replace("{FORGE_DIR}", exten.getForgeDir());
        pattern = pattern.replace("{BUKKIT_DIR}", exten.getBukkitDir());
        pattern = pattern.replace("{MAPPINGS_DIR}", exten.getFmlDir() + "/conf");
        //pattern = pattern.replace("{MC_VERSION}", exten.getVersion());
        pattern = pattern.replace("{METAWORLDS_DIR}", metaworldsDir);
        return pattern;
	}
}
package net.minecraftforge.gradle.dev;

import static net.minecraftforge.gradle.dev.DevConstants.*;

final class MwDevConstants {
	static final String METAWORLDS_PATCH_DIR		= "{METAWORLDS_DIR}/patches";
	
	static final String FORGE_UNIVERSAL_JAR		= "{METAWORLDS_DIR}/universals/forge-{MC_VERSION}-universal.jar";
	static final String MCPC_UNIVERSAL_JAR		= "{METAWORLDS_DIR}/universals/mcpc-plus-{MC_VERSION}-universal.jar";
	
	static final String FORGE_USERDEV_JAR		= "{METAWORLDS_DIR}/universals/forge-{MC_VERSION}-userdev.jar";

	static final String METAWORLDS_JSON_DEV		= "{METAWORLDS_DIR}/jsons/{MC_VERSION}-forge-dev.json";
	static final String METAWORLDS_MCPC_JSON_DEV		= "{METAWORLDS_DIR}/jsons/{MC_VERSION}-mcpc-dev.json";
	
	static final String METAWORLDS_SOURCES 			= "{METAWORLDS_DIR}/src/main/java";
	static final String METAWORLDS_RESOURCES 		= "{METAWORLDS_DIR}/src/main/resources";
	static final String METAWORLDS_TEST_SOURCES 	= "{METAWORLDS_DIR}/src/test/java";
	static final String METAWORLDS_TEST_RES 		= "{METAWORLDS_DIR}/src/test/resources";
	
	static final String ZIP_FORGEINJECTED_METAWORLDS		= "{BUILD_DIR}/mwTmp/minecraft_forgeinjected.zip";
	static final String ZIP_MCPCINJECTED_METAWORLDS		= "{BUILD_DIR}/mwTmp/minecraft_mcpcinjected.zip";
	static final String ZIP_PATCHED_METAWORLDS   	= "{BUILD_DIR}/mwTmp/minecraft_patched.zip";
	
	static final String ECLIPSE_METAWORLDS			= WORKSPACE + "/MetaWorlds";
	static final String ECLIPSE_METAWORLDS_SRC		= ECLIPSE_METAWORLDS + "/src/main/java";
	static final String ECLIPSE_METAWORLDS_RES		= ECLIPSE_METAWORLDS + "/src/main/resources";
	
	static final String MW_BINPATCH_TMP = "{BUILD_DIR}/tmp/mw_binpatches.jar";
	
	static final String MW_BINARIES_TMP = "{BUILD_DIR}/mwTmp/binaries.jar";
	
	static final String FORGE_CLEANED_REOBF_TMP_CLIENT = "{BUILD_DIR}/tmp/recomp_obfed_cleaned_client.jar";
	static final String FORGE_CLEANED_REOBF_TMP_SERVER = "{BUILD_DIR}/tmp/recomp_obfed_cleaned_server.jar";
	static final String FORGE_CLEANED_REOBF_TMP_MERGED = "{BUILD_DIR}/tmp/recomp_obfed_cleaned_merged.jar";
	static final String MW_REOBF_TMP = "{BUILD_DIR}/tmp/recomp_obfed_mw.jar";
	
	static final String FORGE_ON_SITE_CLIENT = "{BUILD_DIR}/tmp/forge_onsite_client.jar";
	static final String FORGE_ON_SITE_SERVER = "{BUILD_DIR}/tmp/forge_onsite_server.jar";
	static final String FORGE_ON_SITE_MERGED = "{BUILD_DIR}/tmp/forge_onsite_merged.jar";
}
